import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.example.SwitchNotation;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SwitchNotationTest {

    SwitchNotation switchNotation = new SwitchNotation();

    @ParameterizedTest
    @MethodSource("provideParametersForCorrectNotationParse")
    void shouldParseCorrectly(String data, String parsedData) {
        assertEquals(parsedData, switchNotation.switchNotation(data));
    }

    private static Stream<Arguments> provideParametersForCorrectNotationParse() {
        return Stream.of(
                Arguments.of( "3,4,+,5,*,3,4,+,-", "(4+(3-(5*(4+3))))"),
                Arguments.of( "3,4,+,5,*", "(5*(4+3))"),
                Arguments.of( "3,4,1,-,+,5,*", "(5*(1-(4+3)))")
        );
    }
}
