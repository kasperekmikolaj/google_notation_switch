package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SwitchNotation {

    private final List<String> OPERATORS = Arrays.asList("-", "+", "*", "/");
    private final String openingBracket = "(";
    private final String closingBracket = ")";

    public String switchNotation(String data) {
        StringBuilder result = new StringBuilder();
        result.append(openingBracket);
        int bracketsCounter = 1;
        List<String> dataAsList = createListFromString(data);
        int nextNumberIndex = findIndexOfLastNumber(dataAsList);
        int nextOperatorIndex = nextNumberIndex+1;
        result
                .append(dataAsList.get(nextNumberIndex))
                .append(dataAsList.get(nextOperatorIndex));

        while(nextNumberIndex!=1) {
            nextNumberIndex = findNumberBefore(nextNumberIndex, dataAsList);
            nextOperatorIndex = findNextOperator(nextOperatorIndex, nextNumberIndex, dataAsList);
            result
                    .append(openingBracket)
                    .append(dataAsList.get(nextNumberIndex))
                    .append(dataAsList.get(nextOperatorIndex));
            bracketsCounter++;
        }
        result.append(dataAsList.get(0));
        appendClosingBrackets(result, bracketsCounter);
        return result.toString();
    }

    private void appendClosingBrackets(StringBuilder builder, int bracketsCounter) {
        builder.append(closingBracket.repeat(bracketsCounter));
    }

    private int findNextOperator(int lastOperatorIndex, int nextNumberIndex, List<String> dataAsList) {
        if (lastOperatorIndex+1 < dataAsList.size() && OPERATORS.contains(dataAsList.get(lastOperatorIndex+1))) {
            return lastOperatorIndex+1;
        } else {
            return nextNumberIndex+1;
        }
    }

    private int findNumberBefore(int lookBefore, List<String> dataAsList) {
        for (int i=lookBefore-1; i >=0; i--){
            if (!OPERATORS.contains(dataAsList.get(i))) {
                return i;
            }
        }
        return -1;
    }

    private List<String> createListFromString(String data) {
        List<String> result = new ArrayList<>();
        for(int i =0; i<data.length(); i++) {
            String nextElem = String.valueOf(data.charAt(i));
            if (!nextElem.equals(",")) {
                result.add(nextElem);
            }
        }
        return result;
    }

    private int findIndexOfLastNumber(List<String> data) {
        return findNumberBefore(data.size()-1, data);
    }
}
